/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import path from 'path';
import type { StorybookConfig } from '@storybook/html-webpack5';

const customRules: any[] = [
  { 
    test: /\.mustache$/,
    loader: path.resolve(__dirname, '../build/webpack/mustache-loader.js'),
  },
  {
    test: /\.less$/,
    use: ['style-loader', 'css-loader', 'less-loader']
  },
];

const config: StorybookConfig = {
  stories: ['../stories/**/*.mdx', '../stories/**/*.stories.@(js|jsx|mjs|ts|tsx)'],
  addons: [
    '@storybook/addon-webpack5-compiler-swc',
    '@storybook/addon-links',
    '@storybook/addon-essentials',
    '@storybook/addon-queryparams',
    '@storybook/addon-interactions',
    '@chromatic-com/storybook',
    '@whitespace/storybook-addon-html',
  ],
  framework: {
    name: '@storybook/html-webpack5',
    options: {},
  },
  docs: {
    autodocs: 'tag',
  },
  staticDirs: ['../static'],
  webpackFinal: async (config) => {
    return {
      ...config,
      module: {
        ...config.module,
        //@ts-ignore
        rules: [...config.module.rules, ...customRules],
      },
      output: { ...config.output, },
      resolve: { ...config.resolve, fallback: { querystring: false }}
    };
  },
  // Append a style tag to head in order to change colors of the svg icons.
  managerHead: (head) => `
${head}
<style>
  #storybook-explorer-tree .sidebar-item[data-selected='false'] svg {
    color: orange;
  }

  #storybook-explorer-tree .sidebar-item[data-selected='true'] svg {
    color: white;
  }
</style>
  `,
};

export default config;
