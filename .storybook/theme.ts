/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { create } from '@storybook/theming/create';

export default create({
  base: 'light',
  brandTitle: 'Solstice Assets',
  brandImage: '/docs/solstice/eclipse-foundation.svg',
  
  // Base Colors
  colorPrimary: '#404040',
  colorSecondary:'#f06c00',
  
  // UI
  appBg: '#ffffff',
  appContentBg: '#ffffff',
  appPreviewBg: '#ffffff',
  appBorderColor: '#eeeeee',
  appBorderRadius: 5,

  textColor: '#4c4d4e',
  textInverseColor: '#0ff',

  barBg: '#efefef',
  
  inputBg: '#efefef',
  inputBorder: '#ddd',
  inputBorderRadius: 5,

  barHoverColor: '#f06c00',
  barSelectedColor: '#f06c00',
});
