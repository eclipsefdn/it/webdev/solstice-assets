/*!
 * Copyright (c) 2018, 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Christopher Guindon <chris.guindon@eclipse-foundation.org>
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import './eclipsefdn.block-summary-item';
import './eclipsefdn.featured-stories';
import './eclipsefdn.solstice';
import './eclipsefdn.match-height';
import './eclipsefdn.scrollup';
import './eclipsefdn.solstice-rss-blog-list';
import './eclipsefdn.solstice-slider';
import './eclipsefdn.time-conversion';
import './eclipsefdn.newsroom-resources.js'
import './eclipsefdn.newsroom-news-tags.js'
import './eclipsefdn.sidebar.js';
import eclipsefdnAdopters from './eclipsefdn.adopters.js';
import eclipsefdnMemberDetail from './eclipsefdn.members-detail';
import eclipsefdnMembersList from './eclipsefdn.members-list';
import eclipsefdnMemberLogo from './eclipsefdn.member-logo';
import eclipsefdnNewsroomResources from './eclipsefdn.newsroom-resources';
import eclipsefdnParticipatingOrganizations from './eclipsefdn.participating-organizations.js';
import eclipsefdnProjectCount from './eclipsefdn.project-count.js';
import eclipsefdnProjects from './eclipsefdn.projects';
import eclipsefdnPromotion from './eclipsefdn.promotion.js';
import eclipsefdnVideoList from './eclipsefdn.video-list.js';
import eclipsefdnWeightedCollaborations from './eclipsefdn.weighted-collaborations';
import eclipsefdnWorkingGroupsList from './eclipsefdn.wgs-list.js'


document.addEventListener('DOMContentLoaded', () => {
  eclipsefdnAdopters.render();
  eclipsefdnMemberDetail.render();
  eclipsefdnMembersList.render();
  eclipsefdnMemberLogo.renderAll();
  eclipsefdnNewsroomResources.render();
  eclipsefdnParticipatingOrganizations.render();
  eclipsefdnProjectCount.render();
  eclipsefdnProjects.render();
  eclipsefdnPromotion.render();
  eclipsefdnVideoList.render();
  eclipsefdnWeightedCollaborations.render();
  eclipsefdnWorkingGroupsList.render();
});
