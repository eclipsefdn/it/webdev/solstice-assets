/**
 * @jest-environment jsdom
 */

/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import eclipsefdnWeightedCollaborations from '../eclipsefdn.weighted-collaborations';
import * as workingGroupsModule from '../../api/eclipsefdn.working-groups';
import * as interestGroupsModule from '../../api/eclipsefdn.interest-groups';

describe('eclipsefdn-weighted-collaborations', () => {
  let getWorkingGroupsSpy;
  let getInterestGroupsSpy;

  beforeEach(() => {
    getWorkingGroupsSpy = jest.spyOn(workingGroupsModule, 'getWorkingGroups');
    getInterestGroupsSpy = jest.spyOn(interestGroupsModule, 'getInterestGroups');
  });

  it('Should render a specific number of collaborations based on count option', async () => {
    document.body.innerHTML = `
      <div class="eclipsefdn-weighted-collaborations" data-count="3"></div>
    `;

    await eclipsefdnWeightedCollaborations.render();

    expect(document.querySelectorAll('.weighted-collaboration').length).toEqual(3);
  });

  it('Should cache the collaborations to session storage depending on the cache option', async () => {
    // First we'll test if cache is set to false.
    document.body.innerHTML = `
      <div class="eclipsefdn-weighted-collaborations" data-cache="false"></div>
    `;

    await eclipsefdnWeightedCollaborations.render();

    expect(Object.keys(window.sessionStorage).length).toEqual(0);

    document.body.innerHTML = `
      <div class="eclipsefdn-weighted-collaborations" data-cache="true"></div>
    `;

    await eclipsefdnWeightedCollaborations.render();

    expect(Object.keys(window.sessionStorage).length).toEqual(1);
  });

  it('Template "default": Should skeleton load depending on the skeleton-load option', async () => {
    // First we are testing for the skeleton-load option when disabled.
    document.body.innerHTML = `
      <div class="eclipsefdn-weighted-collaborations" data-skeleton-load="false" data-count="2"></div>
    `;

    eclipsefdnWeightedCollaborations.render();

    let loadingSpinnerElements = document.querySelectorAll('.loading-spinner');

    // There should only be one loading spinner since data-skeleton-load is
    // false.
    expect(loadingSpinnerElements.length).toEqual(1);

    // Now we are testing for the skeleton-load option when enabled.
    document.body.innerHTML = `
      <div class="eclipsefdn-weighted-collaborations" data-skeleton-load="true" data-count="2"></div>
    `;

    eclipsefdnWeightedCollaborations.render();
    loadingSpinnerElements = document.querySelectorAll('.loading-spinner');

    // There should be two loading spinners since data-count is 2.
    expect(loadingSpinnerElements.length).toEqual(2);
  });
});
