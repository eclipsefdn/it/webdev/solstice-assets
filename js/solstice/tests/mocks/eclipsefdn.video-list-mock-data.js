export const playlists = [
  {
    id: 'PLy7t4z5SYNaTao4IDYllEZ7NGIlPbCbpr',
    title: 'Committer Training',
    description: 'This is a short description on committer training',
    publishedAt: '2022-12-08T08:36:42Z',
    thumbnails: {
      medium: {
        url: 'https://i.ytimg.com/vi/Th1pC-yT1Ac/mqdefault.jpg',
        width: 320,
        height: 180,
      },
      high: {
        url: 'https://i.ytimg.com/vi/Th1pC-yT1Ac/hqdefault.jpg',
        width: 480,
        height: 360,
      },
      standard: {
        url: 'https://i.ytimg.com/vi/Th1pC-yT1Ac/sddefault.jpg',
        width: 640,
        height: 480,
      },
      maxres: {
        url: 'https://i.ytimg.com/vi/Th1pC-yT1Ac/maxresdefault.jpg',
        width: 1280,
        height: 720,
      },
      default: {
        url: 'https://i.ytimg.com/vi/Th1pC-yT1Ac/default.jpg',
        width: 120,
        height: 90,
      },
    },
    channelId: 'UCej18QqbZDxuYxyERPgs2Fw',
    channelTitle: 'Eclipse Foundation',
    player: {
      embedHtml:
        '<iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLy7t4z5SYNaTxmWWrnLYshYKaSLhRKNk-" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
    },
  },
  {
    id: 'PLy7t4z5SYNaTxmWWrnLYshYKaSLhRKNk-',
    title: 'Celebrating Theia 5th Anniversary',
    description: '',
    publishedAt: '2022-12-08T08:36:42Z',
    thumbnails: {
      medium: {
        url: 'https://i.ytimg.com/vi/Th1pC-yT1Ac/mqdefault.jpg',
        width: 320,
        height: 180,
      },
      high: {
        url: 'https://i.ytimg.com/vi/Th1pC-yT1Ac/hqdefault.jpg',
        width: 480,
        height: 360,
      },
      standard: {
        url: 'https://i.ytimg.com/vi/Th1pC-yT1Ac/sddefault.jpg',
        width: 640,
        height: 480,
      },
      maxres: {
        url: 'https://i.ytimg.com/vi/Th1pC-yT1Ac/maxresdefault.jpg',
        width: 1280,
        height: 720,
      },
      default: {
        url: 'https://i.ytimg.com/vi/Th1pC-yT1Ac/default.jpg',
        width: 120,
        height: 90,
      },
    },
    channelId: 'UCej18QqbZDxuYxyERPgs2Fw',
    channelTitle: 'Eclipse Foundation',
    player: {
      embedHtml:
        '<iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLy7t4z5SYNaTxmWWrnLYshYKaSLhRKNk-" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
    },
  },
];
