/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */


import eclipsefdnPromotion from '../eclipsefdn.promotion';
import * as newsroomAPIModule from '../../api/eclipsefdn.newsroom';
import { mockAds } from '../../api/tests/mocks/newsroom/mock-data';

describe('eclipsefdn-promo-content', () => {
  let getAdsSpy;

  beforeEach(() => {
    getAdsSpy = jest.spyOn(newsroomAPIModule, 'getAds');
  });

  afterAll(() => {
  });

  it('Renders ads successfully', async () => {
    getAdsSpy.mockImplementation(() => [mockAds.api, null]);
    document.body.innerHTML = `
      <div 
        class="eclipsefdn-promo-content" 
        data-ad-format="ads_top_leaderboard"
        data-ad-publish-to="eclipse_org_home"
      ></div>
    `;

    await eclipsefdnPromotion.render();

    const adImage = document.querySelector('.eclipsefdn-promo-content img');
    expect(adImage).toBeDefined();
    getAdsSpy.mockRestore();
  });

  it('Should do nothing if no widget is on the page', async () => {
    document.body.innerHTML = '';

    await eclipsefdnPromotion.render();

    expect(document.body.innerHTML).toBe('');
    expect(getAdsSpy).not.toHaveBeenCalled();
  });

  it('Renders ads with the specified format', async () => {
    const adFormat = 'ads_square';
    const squareFormatMockAds = mockAds.api.filter((ad) => ad.format === adFormat);

    getAdsSpy.mockImplementation(() => [squareFormatMockAds, null]);

    document.body.innerHTML = `
      <div
        class="eclipsefdn-promo-content"
        data-ad-publish-to="eclipse_org_home"
        data-ad-format="${adFormat}"
      ></div>
    `;  

    await eclipsefdnPromotion.render();
    
    // Expect getAdsSpy to have only bene called with the ads_square format.
    expect(getAdsSpy).toHaveBeenCalledWith(
      expect.objectContaining({ 
        formats: { 'ads_square': '1' }
      })
    );

    getAdsSpy.mockRestore();
  });


  it('Should error when ad format is missing', async () => {
    const consoleSpy = jest.spyOn(console, 'error').mockImplementation(() => {});

    document.body.innerHTML = `
      <div
        class="eclipsefdn-promo-content"
        data-ad-publish-to="eclipse_org_home"
      ></div>
    `;

    await eclipsefdnPromotion.render();

    expect(consoleSpy).toHaveBeenCalledWith('The `data-ad-format` attribute is required for eclipsefdn-promo-content.');

    consoleSpy.mockRestore();
  });

  it('Should error when publish-to option is missing', async () => {
    const consoleSpy = jest.spyOn(console, 'error').mockImplementation(() => {});

    document.body.innerHTML = `
      <div
        class="eclipsefdn-promo-content"
        data-ad-format="ads_top_leaderboard"
      ></div>
    `;

    await eclipsefdnPromotion.render();

    expect(consoleSpy).toHaveBeenCalledWith('The `data-ad-publish-to` attribute is required for eclipsefdn-promo-content.');

    consoleSpy.mockRestore();
  });
});
