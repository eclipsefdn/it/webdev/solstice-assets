/**
 * @jest-environment jsdom
 */

/*
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

describe('User Interface', () => {
  describe('Widgets', () => {
    require('./eclipsefdn.members-detail.test');
    require('./eclipsefdn.members-list.test');
    require('./eclipsefdn.member-logo.test');
    require('./eclipsefdn.newsroom-resources.test');
    require('./eclipsefdn.project-count.test');
    require('./eclipsefdn.projects.test');
    require('./eclipsefdn.promotion.test');
    require('./eclipsefdn.video-list.test');
    require('./eclipsefdn.weighted-collaborations.test');
  });

  describe('Layout', () => {
    require('./eclipsefdn.header.test');
  });

  describe('Helpers', () => {
    require('./render-template.test');
  });
});
