/**
 * @jest-environment jsdom
 */

/*
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import eclipsefdnMembersList from '../eclipsefdn.members-list';
import * as interestGroupAPI from '../../api/eclipsefdn.interest-groups';
import { generateMockOrganization } from '../../api/tests/mocks/interest-groups/mock-data';

describe('eclipsefdn-members-list', () => {
  let getInterestGroupParticipatingOrganizationsSpy;

  beforeEach(() => {
    getInterestGroupParticipatingOrganizationsSpy = jest.spyOn(
      interestGroupAPI,
      'getInterestGroupParticipatingOrganizations'
    );
  });

  it('Renders interest group members', async () => {
    const shortId = 'openmobility';

    getInterestGroupParticipatingOrganizationsSpy.mockReturnValue([
      Array.from({ length: 10 }).map(generateMockOrganization),
      null,
    ]);

    document.querySelector('body').innerHTML = `
        <ul
            class="eclipsefdn-members-list"
            data-type="interest-group"
            data-id="${shortId}"
            data-ml-template="only-logos"
        >
        </ul>
    `;

    eclipsefdnMembersList.render();

    // This hack is needed to ensure the members-list has rendered but it is not guaranteed to work.
    // This should be replaced with `await render()` once eclipsefdn-members-list uses
    // `renderTemplate`.
    await new Promise(resolve => setTimeout(() => resolve(), 1000));
    
    // Expect getInterestGroupParticiatingOrganizations to have been called
    // with the project short id of the interest group.
    expect(getInterestGroupParticipatingOrganizationsSpy).toHaveBeenCalledWith({
      interestGroupId: shortId,
    });

    // Expect logos to have been rendered
    const logoElements = Array.from(document.querySelectorAll('.eclipsefdn-members-list img'));
    expect(logoElements.length).not.toBe(0);
  });

  it('Shows member as text when no logo is present', async () => {
    const shortId = 'openmobility';

    // Mutating the first element of participating organizations to ensure the web logo is `null`.
    // Everything else is set to a url mock.
    let participatingOrganizations = Array
      .from({ length: 10 })
      .map(generateMockOrganization)
    participatingOrganizations[0].logos.web = null;

    getInterestGroupParticipatingOrganizationsSpy.mockReturnValue([
      participatingOrganizations,
      null
    ]);

    document.querySelector('body').innerHTML = `
      <ul
        class="eclipsefdn-members-list"
        data-type="interest-group"
        data-id="${shortId}"
        data-ml-template="only-logos"
      >
      </ul>
    `;

    eclipsefdnMembersList.render();
    
    // This hack is needed to ensure the members-list has rendered but it is not guaranteed to work.
    // This should be replaced with `await render()` once eclipsefdn-members-list uses
    // `renderTemplate`.
    await new Promise(resolve => setTimeout(() => resolve(), 1000));

    // We expect one of the elements to not have a logo. So we subtract one.
    const expectedLogoCount = participatingOrganizations.length - 1;
    const renderedLogoCount = document.querySelectorAll('.eclipsefdn-members-list img').length;

    const placeholderTextElement = document.querySelector(`.eclipsefdn-members-list .logo-placeholder-text`);

    expect(renderedLogoCount).toEqual(expectedLogoCount);
    expect(placeholderTextElement.innerHTML.trim()).toEqual(participatingOrganizations[0].name);
  });

});
