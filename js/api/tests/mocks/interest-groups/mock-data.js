/*
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { faker } from '@faker-js/faker';
import { transformSnakeToCamel } from '../../../utils';

// Names, usernames, and organization names come from generated mock data.

const generateLevel = () => {
  const level = faker.helpers.arrayElement(['SD', 'AP', 'AS']);

  //@todo: move outside of function
  const descriptions = {
    SD: 'Strategic Member',
    AP: 'Contributing Member',
    AS: 'Associate Member',
  }

  return {
    level,
    description: descriptions[level], 
    sortOrder: (faker.number.int({ min: 1, max: 9 }) * 100).toString(),
  }
}

export const generateMockOrganization = () => {
  return {
    organizationId: faker.number.int({ min: 100, max: 9999 }),
    name: faker.company.name(),
    logos: {
      print: null,
      web: faker.image.urlPlaceholder(),
    },
    levels: Array.from({ length: faker.number.int({ min: 1, max: 5 }) }, generateLevel),
  }
};

const generateMockPerson = () => {
  return {
    username: faker.internet.userName(),
    fullName: faker.person.fullName(),
    organization: {
      id: faker.number.int({ min: 100, max: 9999 }),
      name: faker.company.name(),
      url: faker.internet.url(),
      documents: { mcca: faker.datatype.boolean() },
    },
  }
}

const interestGroupNames = ['openMobility', 'Models for privacy engineering'];

const generateMockInterestGroupResponse = (name) => {
  return {
    id: faker.number.int({ min: 100, max: 9999 }),
    title: name,
    description: {
      summary: '',
      full: faker.lorem.paragraphs(),
    },
    logo: faker.image.urlPlaceholder(),
    scope: {
      summary: '',
      full: faker.lorem.paragraphs(),
    },
    leads: Array.from({ length: faker.number.int({ min: 1, max: 5 }) }, generateMockPerson),
    participants: Array.from({ length: faker.number.int({ min: 1, max: 5 }) }, generateMockPerson),
    project_id: 'foundation.ig.' + name.toLowerCase().replaceAll(' ', '-'),
    short_project_id: name.toLowerCase().replaceAll(' ', '-'),
    state: faker.helpers.arrayElement(['active', 'proposal', 'archived']),
    gitlab: { projectGroup: '', ignoredSubGroups: [] },
    resources: { website: '', members: '' },
    mailing_list: '',
  }
}

const mockInterestGroupResponses = interestGroupNames.map(generateMockInterestGroupResponse);

export const mockInterestGroups = {
  /** The mock data in the shape of the API response */
  api: mockInterestGroupResponses,
  /** The mock data in the shape of the model used by solstice assets */
  model: transformSnakeToCamel(mockInterestGroupResponses),
}

