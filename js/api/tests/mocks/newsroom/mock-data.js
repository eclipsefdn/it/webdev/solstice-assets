/*
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

// No personal data used in this mock data. Names and usernames have been
// generated.

import { faker } from '@faker-js/faker';
import { transformSnakeToCamel } from '../../../../api/utils';

const generateMockAdResponse = () => {
  const id = faker.number.int(5).toString();
  return {
    id,
    campaign_name: id,
    url: faker.internet.url(),
    image: faker.image.urlPlaceholder({ format: 'jpg', text: 'Mock Ad' }),
    member_organization_id: '',
    type: faker.helpers.arrayElement(['strat_ad', 'paid']),
    format: faker.helpers.arrayElement(['ads_square', 'ads_top_leaderboard']),
    weight: faker.number.int({ min: 1, max: 2 }).toString(),
    groups: '',
    publish_to: faker.helpers.arrayElement(['eclipse_org_home', 'eclipse_org_downloads']),
  };
};

const mockAdsResponses = Array.from({ length: 10 }).map(generateMockAdResponse);

export const mockAds = {
  /** The mock data in the shape of the API response */
  api: mockAdsResponses,
  /** The mock data in the shape of the model used by solstice assets */
  model: transformSnakeToCamel(mockAdsResponses),
}

export const mockNewsroomResources = [
  {
    "id": "39384",
    "title": "Eclipse BaSyx Bridges the Middleware Gap for Industry 4.0",
    "date": "2023-03-10T14:44:49Z",
    "body": "Learn how the Eclipse Foundation is enabling the creation of Industry 4.0 solutions.",
    "direct_link": "https://outreach.eclipse.foundation/industry40-middleware-basyx",
    "landing_link": "https://outreach.eclipse.foundation/industry40-middleware-basyx",
    "image": "https://newsroom.eclipse.org/sites/default/files/styles/large/public/resources/images/Eclipse%20BaSyx%20Thumb.png?itok=cvv-GHcf",
    "resource_type": "case_study",
    "author": {
      "full_name": "Sandi Lough",
      "username": "slough0"
    },
    "publish_to": [
      "eclipse_org",
      "research"
    ]
  },
  {
    "id": "38479",
    "title": "From DevOps to EdgeOps: A Vision for Edge Computing",
    "date": "2021-10-07T18:35:01Z",
    "body": "",
    "direct_link": "https://f.hubspotusercontent10.net/hubfs/5413615/Eclipse%20Foundation%20EdgeOps%20White%20Paper.pdf",
    "landing_link": "https://outreach.eclipse.foundation/edge-computing-edgeops-white-paper",
    "image": "https://newsroom.eclipse.org/sites/default/files/styles/large/public/resources/images/from-devops-to-edgeops.jpg?itok=slWEDQ-e",
    "resource_type": "white_paper",
    "author": {
      "full_name": "Anthony Spollen",
      "username": "aspollen8"
    },
    "publish_to": [
      "eclipse_org",
      "edge_native"
    ]
  },
  {
    "id": "38478",
    "title": "OpenADX Manifesto",
    "date": "2021-10-07T18:32:40Z",
    "body": "",
    "direct_link": "https://openadx.eclipse.org/resources/OpenADx-Manifesto-v12-2020.pdf",
    "landing_link": "",
    "image": "https://newsroom.eclipse.org/sites/default/files/styles/large/public/resources/images/OpenADX.PNG?itok=aerORg6y",
    "resource_type": "white_paper",
    "author": {
      "full_name": "Mic Santino",
      "username": "msantiop"
    },
    "publish_to": [
      "eclipse_org",
      "openadx"
    ]
  },
  {
    "id": "39381",
    "title": "Sparkplug: The Open Specification Critical to Achieving ROI in the Industrial Internet of Things",
    "date": "2023-03-08T12:12:51Z",
    "body": "",
    "direct_link": "https://www.hivemq.com/solutions/hivemq-mqtt-sparkplug-open-specification-for-iiot-whitepaper/",
    "landing_link": "",
    "image": "https://newsroom.eclipse.org/sites/default/files/styles/large/public/resources/images/Untitled%20design.png?itok=uDycYlCg",
    "resource_type": "white_paper",
    "author": {
      "full_name": "Anonymous",
      "username": ""
    },
    "publish_to": [
      "sparkplug"
    ]
  }
]
