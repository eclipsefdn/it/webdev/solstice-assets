/*
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { rest } from 'msw';
import { mockPlaylists } from './mock-data';

const apiUrls = {
  mediaLink: 'https://api.eclipse.org/media',
};

const handlers = [
  rest.get(`${apiUrls.mediaLink}/youtube/playlists/:playlistId`, (req, res, ctx) => {

    const playlist = mockPlaylists.find(p => p.id === req.params.playlistId);

    return res(
      ctx.status(200),
      ctx.json(playlist)
    );
  }),

  rest.get(`${apiUrls.mediaLink}/youtube/playlists`, (req, res, ctx) => {
    const channel = req.url.searchParams.get('channel');

    const channelNameMap = {
      'eclipsefdn': 'Eclipse Foundation'
    }

    const playlists = mockPlaylists.filter(p => p.snippet.channel_title === channelNameMap[channel]);

    return res(
      ctx.status(200),
      ctx.json(playlists)
    )
  })
];

export default handlers;
