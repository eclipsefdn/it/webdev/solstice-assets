/*
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

export const mockPlaylists = [
    {
      id: 'PLy7t4z5SYNaTao4IDYllEZ7NGIlPbCbpr',
      etag: 'BLxv1xItqZPzMG7C9gvL3wyl9Gc',
      snippet: {
        published_at: '2022-12-20T18:56:18Z',
        channel_id: 'UCej18QqbZDxuYxyERPgs2Fw',
        title: 'Committer Training',
        thumbnails: {
          medium: {
            url: 'https://i.ytimg.com/vi/J7ugyepyIvU/mqdefault.jpg',
            width: 320,
            height: 180,
          },
          high: {
            url: 'https://i.ytimg.com/vi/J7ugyepyIvU/hqdefault.jpg',
            width: 480,
            height: 360,
          },
          standard: {
            url: 'https://i.ytimg.com/vi/J7ugyepyIvU/sddefault.jpg',
            width: 640,
            height: 480,
          },
          maxres: {
            url: 'https://i.ytimg.com/vi/J7ugyepyIvU/maxresdefault.jpg',
            width: 1280,
            height: 720,
          },
          default: {
            url: 'https://i.ytimg.com/vi/J7ugyepyIvU/default.jpg',
            width: 120,
            height: 90,
          },
        },
        channel_title: 'Eclipse Foundation',
        localized: {
          title: 'Committer Training',
          description: '',
        },
        description: '',
      },
      player: {
        embed_html:
          '<iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLy7t4z5SYNaTao4IDYllEZ7NGIlPbCbpr" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
      },
    },
    {
      id: 'PLy7t4z5SYNaTxmWWrnLYshYKaSLhRKNk-',
      etag: 'GIlt1wmWmIlDvAhso8A6Q0cOs1o',
      snippet: {
        published_at: '2022-12-08T08:36:42Z',
        channel_id: 'UCej18QqbZDxuYxyERPgs2Fw',
        title: 'Celebrating Theia 5th Anniversary',
        thumbnails: {
          medium: {
            url: 'https://i.ytimg.com/vi/Th1pC-yT1Ac/mqdefault.jpg',
            width: 320,
            height: 180,
          },
          high: {
            url: 'https://i.ytimg.com/vi/Th1pC-yT1Ac/hqdefault.jpg',
            width: 480,
            height: 360,
          },
          standard: {
            url: 'https://i.ytimg.com/vi/Th1pC-yT1Ac/sddefault.jpg',
            width: 640,
            height: 480,
          },
          maxres: {
            url: 'https://i.ytimg.com/vi/Th1pC-yT1Ac/maxresdefault.jpg',
            width: 1280,
            height: 720,
          },
          default: {
            url: 'https://i.ytimg.com/vi/Th1pC-yT1Ac/default.jpg',
            width: 120,
            height: 90,
          },
        },
        channel_title: 'Eclipse Foundation',
        localized: {
          title: 'Celebrating Theia 5th Anniversary',
          description: '',
        },
        description: '',
      },
      player: {
        embed_html:
          '<iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLy7t4z5SYNaTxmWWrnLYshYKaSLhRKNk-" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
      },
    },
    {
      id: 'PLy7t4z5SYNaT7XdfANFx72-WwD4-nJjlj',
      etag: 'WsiO9VinhoxqiyTWNa-n9cdHK7I',
      snippet: {
        published_at: '2022-12-05T17:56:26Z',
        channel_id: 'UCej18QqbZDxuYxyERPgs2Fw',
        title: 'Theia Community Release',
        thumbnails: {
          medium: {
            url: 'https://i.ytimg.com/vi/MUIZT2wOkmw/mqdefault.jpg',
            width: 320,
            height: 180,
          },
          high: {
            url: 'https://i.ytimg.com/vi/MUIZT2wOkmw/hqdefault.jpg',
            width: 480,
            height: 360,
          },
          standard: {
            url: 'https://i.ytimg.com/vi/MUIZT2wOkmw/sddefault.jpg',
            width: 640,
            height: 480,
          },
          maxres: {
            url: 'https://i.ytimg.com/vi/MUIZT2wOkmw/maxresdefault.jpg',
            width: 1280,
            height: 720,
          },
          default: {
            url: 'https://i.ytimg.com/vi/MUIZT2wOkmw/default.jpg',
            width: 120,
            height: 90,
          },
        },
        channel_title: 'Eclipse Foundation',
        localized: {
          title: 'Theia Community Release',
          description: '',
        },
        description: '',
      },
      player: {
        embed_html:
          '<iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLy7t4z5SYNaT7XdfANFx72-WwD4-nJjlj" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
      },
    },
  ];