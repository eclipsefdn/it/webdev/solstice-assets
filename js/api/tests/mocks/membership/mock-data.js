/*
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { faker } from '@faker-js/faker';
import { organizationMapper, organizationProductMapper, projectSlimMapper } from '../../../eclipsefdn.membership';
import { mockProjects } from '../projects/mock-data';

// Guarantees the following properties to be part of the mock data set.
export const guarantees = {
  workingGroups: ['jakarta-ee'],
}

const levelDescriptionMap = {
  AP: 'Contributing Member',
  AS: 'Associate Member',
  SD: 'Strategic Member',
  WGSAP: 'Guest Member',
}

const generateMockOrganization = (options) => {
  const workingGroup = options?.workingGroup;

  const memberSince = faker.date.past();
  const wgpaLevels = faker.helpers.arrayElements(['WGSAP']);

  const level = options?.level ?? faker.helpers.arrayElement(['AP', 'SD', 'AS']);

  return {
    organization_id: faker.number.int({ min: 500, max: 2000 }),
    name: faker.company.name(),
    member_since: `${memberSince.getFullYear()}-${memberSince.getMonth() + 1}-${memberSince.getDate()}`,
    description: {
      long: faker.lorem.paragraphs({ min: 1, max: 3 }),
    },
    logos: {
      print: null,
      web: faker.image.url(),
    },
    website: faker.internet.url(),
    levels: [level]
      .map((level) => ({ 
        level, 
        description: levelDescriptionMap[level], 
        sort_order: '500' 
      })),
    wgpas: wgpaLevels.map((level) => ({
      document_id: faker.string.uuid(),
      description: levelDescriptionMap[level],
      level,
      // All the WGPAs will be for one WG. This is an unrealistic scenario but
      // it is satisfactory for a mock.
      working_group: workingGroup || faker.helpers.arrayElement(['jakarta-ee', 'sdv', 'sparkplug']),
    })),
  };
};

const mockOrganizationResponses = [
  ...Array
    .from({ length: 6 })
    .map(generateMockOrganization), 
  // Concatenate a few organizations which are members of Jakarta EE
  ...Array
    .from({ length: 3 })
    .map(() => generateMockOrganization({ workingGroup: 'jakarta-ee' })),
  generateMockOrganization({ level: 'SD' }),
  generateMockOrganization({ level: 'AP' }),
  generateMockOrganization({ level: 'AS' }),
]; 

export const mockOrganizations = {
  api: mockOrganizationResponses,
  model: mockOrganizationResponses.map(organizationMapper),
};

const mockOrganizationByProjectResponses = mockProjects.api
  .map((project) => project.project_id)
  .reduce((responses, projectId) => {
    // Mutate the accumulator "responses" to include a random amount of
    // projects from the organization mocks. 
    // Many-to-many between projects and organizations.
    responses[projectId] = faker.helpers.arrayElements(mockOrganizationResponses);
    return responses;
  }, {});

export const mockOrganizationsByProject = {
  api: mockOrganizationByProjectResponses,
  model: mockOrganizationByProjectResponses,
}

const generateMockOrganizationProduct = (options) => {
  if (!options.organizationId) throw new TypeError('Organization Id must be passed to generateMockOrganizationProduct');

  const organizationId = options.organizationId;
  
  return {
    product_id: faker.number.int({ min: 100, max: 999 }),
    organization_id: organizationId,
    name: faker.commerce.productName(),
    description: faker.commerce.productDescription(),
    product_url: faker.internet.url(),
  }
}

const mockOrganizationProductResponses = mockOrganizations.api.reduce((responses, organization) => {
  responses[organization.organization_id] = Array
    .from({ length: 3 })
    .map(() => generateMockOrganizationProduct({ organizationId: organization.organization_id }));
  return responses;
}, {});

// Maps an object's values via a map function. Similar to Array.map, this does
// not mutate the original object.
const mapObject = (object, mapFn) => {
  // structuredClone() is not available in JSDOM so we are using JSON.stringify
  // and JSON.parse as a hack for deep copy.
  const copy = JSON.parse(JSON.stringify(object));
  Object.keys(object).forEach((key) => {
    copy[key] = mapFn(copy[key]);
  })

  return copy;
}

export const mockOrganizationProducts = {
  api: mockOrganizationProductResponses,
  model: mapObject(mockOrganizationProductResponses, (products) => products.map(organizationProductMapper)), 
}

const generateMockOrganizationProject = () => {
  const topLevelProjectName = faker.commerce.productName();
  const topLevelProjectId = topLevelProjectName.replaceAll(' ', '-').toLowerCase();
  const projectName = faker.commerce.productName();
  const projectId = `${topLevelProjectId}.${projectName.replaceAll(' ', '-').toLowerCase()}`;

  return {
    project_id: projectId,
    name: projectName,
    level: faker.number.int({ min: 1, max: 3 }),
    parent_project_id: topLevelProjectId,
    description: faker.helpers.maybe(() => faker.lorem.paragraph()) ?? '',
    url_download: faker.internet.url(),
    url_index: `https://projects.eclipse.org/projects/${projectId}`,
    date_active: '2016-09-22T00:00:00Z',
    sort_order: faker.number.int({ min: 0, max: 1 }),
    active: faker.datatype.boolean(0.75),
    bugs_name: '',
    project_phase: faker.helpers.arrayElement(['Regular', 'Incubation']),
    disk_quota_gb: 2,
    component: false,
    standard: false,
  };
};

const mockOrganizationProjectResponses = mockOrganizations.api.reduce((responses, organization) => {
  responses[organization.organization_id] = Array
    .from({ length: 3 })
    .map(() => generateMockOrganizationProject());

  return responses;
}, {})

export const mockOrganizationProjects = {
  api: mockOrganizationProjectResponses,
  model: mapObject(mockOrganizationProjectResponses, (projects) => projects.map(projectSlimMapper)),
};

