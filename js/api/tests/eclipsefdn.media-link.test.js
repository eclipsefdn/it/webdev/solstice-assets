/*
 * Copyright (c) 2022, 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { playlistMapping, youtube } from '../eclipsefdn.media-link';
import { mockPlaylists } from './mocks/media-link/mock-data';

const mappedPlaylist = {
    id: 'PLy7t4z5SYNaTxmWWrnLYshYKaSLhRKNk-',
    title: 'Celebrating Theia 5th Anniversary',
    description: '',
    publishedAt: '2022-12-08T08:36:42Z',
    thumbnails:  {
        medium: {
            url: 'https://i.ytimg.com/vi/Th1pC-yT1Ac/mqdefault.jpg',
            width: 320,
            height: 180,
            },
            high: {
            url: 'https://i.ytimg.com/vi/Th1pC-yT1Ac/hqdefault.jpg',
            width: 480,
            height: 360,
            },
            standard: {
            url: 'https://i.ytimg.com/vi/Th1pC-yT1Ac/sddefault.jpg',
            width: 640,
            height: 480,
            },
            maxres: {
            url: 'https://i.ytimg.com/vi/Th1pC-yT1Ac/maxresdefault.jpg',
            width: 1280,
            height: 720,
            },
            default: {
            url: 'https://i.ytimg.com/vi/Th1pC-yT1Ac/default.jpg',
            width: 120,
            height: 90,
            }
    },
    channelId: 'UCej18QqbZDxuYxyERPgs2Fw',
    channelTitle: 'Eclipse Foundation',
    player: {
        embedHtml: '<iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLy7t4z5SYNaTxmWWrnLYshYKaSLhRKNk-" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
    }
}

describe('Media Link API', () => {

  it('Maps playlist keys from API to model', () => {
    const expected = mappedPlaylist;
    // Get playlist which has the same ID as the playlist from our expected result
    const playlistFromAPI = mockPlaylists.find(p => p.id === expected.id);

    if (!playlistFromAPI) throw new Error('The expected playlist\'s ID does not exist in the mock data for playlists');

    const result = playlistMapping(playlistFromAPI);

    expect(result).toMatchObject(expected);
  });

  it('Fetches a single playlist', async () => {
    const idToFind = mockPlaylists[0].id;

    const [playlist, error] = await youtube.getPlaylist(idToFind);
    
    expect(playlist).toHaveProperty('id', idToFind);
    expect(playlist).toHaveProperty('title', mockPlaylists[0].snippet.title);
    expect(error).toBeNull();
  });

  it('Fetches multiple playlists', async () => {
    const playlistsToFind = mockPlaylists.slice(0, 2);
    const idsToFind = playlistsToFind.map(p => p.id);

    const [playlists, errors] = await youtube.getPlaylists(idsToFind);

    expect(playlists).toHaveLength(2);
    expect(errors).toHaveLength(2);
    expect(playlists[0]).toHaveProperty('id', idsToFind[0]);
    expect(playlists[1]).toHaveProperty('id', idsToFind[1]);
    expect(playlists[0]).toHaveProperty('title', playlistsToFind[0].snippet.title);
    expect(errors[0]).toBeNull();
  });

  it('Fetches playlists from a channel', async () => {
    const [playlists, error] =  await youtube.getPlaylistsFromChannel('eclipsefdn');

    expect(playlists).not.toHaveLength(0);
    expect(playlists[0]).toHaveProperty('channelTitle', 'Eclipse Foundation');
    expect(error).toBeNull();
  });
});
