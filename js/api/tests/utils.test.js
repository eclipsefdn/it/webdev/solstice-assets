/**
 * @jest-environment jsdom
 */

/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *  Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { fetchXML } from '../utils';

const xmlEndpoint = new URL('https://planeteclipse.org/planet/rss20.xml');
const fakeEndpoint = new URL('https://www.eclipse.org/not/a/real/endpoint.xml');

describe('Utilities', () => {
  describe('fetchXML', () => {
    it('Returns an XML document on a valid fetch', async () => {
      return fetchXML(xmlEndpoint)
        .then((data) => {
          expect(data).toBeInstanceOf(Document);
          expect(data.querySelector('parsererror')).toBeFalsy();
        });
    });

    it('Throws an error if an invalid endpoint is given', () => {
      return expect(fetchXML(fakeEndpoint)).rejects.toThrow();
    });
  });
});
