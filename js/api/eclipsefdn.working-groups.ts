/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import 'isomorphic-fetch';
import { transformSnakeToCamel } from './utils';
import { WorkingGroup } from '../types/models';
import { APIResponse, HttpError } from '../types/api';

const apiBasePath = 'https://api.eclipse.org/working-groups';

export const getWorkingGroups = async () => {
  try {
    const response = await fetch(apiBasePath);
    const data = await response.json();
    
    const workingGroups = data.map(transformSnakeToCamel);

    return [workingGroups, null];
  } catch (error) {
    return [null, error];
  }
}

/**
  * Retrieves working groups that are associated with an organization.
  *
  * @param organizationId - The ID of the organization
  * @returns Working groups
  */
export const getOrganizationWorkingGroups = async (organizationId: number): Promise<APIResponse<WorkingGroup[]>> => {
  try {
    if (!organizationId) {
      throw new TypeError('No organization id was provided for fetching organization working groups');
    }

    const response = await fetch(`${apiBasePath}/organization/${organizationId}`);
    if (!response.ok) throw new HttpError(response.status, `could not fetch working groups for organization ${organizationId}`);
  
    const data = await response.json();
    const workingGroups = data.map(transformSnakeToCamel) as WorkingGroup[];

    return [workingGroups, null];
  } catch (error) {
    return [null, error];
  }
}

