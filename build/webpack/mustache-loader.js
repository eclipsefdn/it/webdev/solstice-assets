/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

const transformMustache = require('../transform-mustache');

/** A webpack loader to transform mustache templates into callable functions. 
  * 
  * @param {string} sourceText - the contents of the file being processed.
  * @returns {string} JavaScript code as a string.
  */
module.exports = function(sourceText) {
  return transformMustache(sourceText, this.resourcePath);
}
