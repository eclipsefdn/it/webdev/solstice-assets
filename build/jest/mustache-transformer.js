/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

const transformMustache = require('../transform-mustache');

// This file is used as a Jest transformer for mustache and mustache partial
// files. This should work in similar ways to the bundler config.
module.exports = {
  process(sourceText, sourcePath, _options) {
    return {
      code: transformMustache(sourceText, sourcePath), 
    }
  }
}
