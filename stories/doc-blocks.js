/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React from 'react';
import { Unstyled } from '@storybook/blocks';

export const Example = ({ children }) => {
  return (
    <Unstyled>
      <div style={{marginBottom: '2rem', backgroundColor: '#fff', borderRadius: '0.5rem', padding: '2rem', border: '1px solid #eaeaea', boxShadow: '0 0 8px #00000011' }}>
        {children}
      </div>
    </Unstyled>
  );
}
