/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import '../../less/astro/main.less';
import type { Meta, StoryObj } from '@storybook/html';
import '../../js/bootstrap';
import { html, container } from '../utils';

const meta: Meta = {
  title: 'Static Components/Timeline',
  decorators: [container()],
};

export default meta;

type Story = StoryObj<any>;

export const Timeline: Story = {
  argTypes: {
    orientation: { control: 'radio', options: ['horizontal', 'vertical'] },
    placement: { 
      control: 'radio', 
      options: ['opposite', 'end'],
    },
  },
  args: {
    orientation: 'vertical',
    placement: 'opposite',
  },
  render: ({ orientation, placement }) => {
    if (orientation === 'horizontal' && placement === 'opposite') {
      return html`
        <div class="alert alert-info">
          This combination of options is not yet supported.
        </div>
      `;
    }

    return html`
      <div class="timeline timeline-${orientation} timeline-${placement}">
        <div class="timeline-item">
          <div>First Item</div> 
        </div>
        <div class="timeline-item">
          <div>Second Item</div> 
        </div>
        <div class="timeline-item">
          <div>Third Item</div> 
        </div>
      </div>
    `
  },
}
