/*
 * Copyright (c) 2025 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import '../../less/astro/main.less';
import type { Meta, StoryObj } from '@storybook/html';
import '../../js/bootstrap';
import { container } from '../utils';

const meta: Meta = {
  title: 'Layout/Sidebar',
  decorators: [container()],
};

export default meta;

type Story = StoryObj<any>;

export const Sidebar: Story = {
  render: () => {
    return (`
      <div class="row">
        <div class="col-md-6 col-md-offset-18">
        <!-- The <aside> is the actual sidebar markup. Above is markup to get
         the page layout in order. -->
          <aside class="main-sidebar-default-margin" id="main-sidebar">
            <ul class="ul-left-nav" id="leftnav" role="tablist" aria-multiselectable="true">
              <li class="main-sidebar-main-item main-sidebar-item-indented separator">
                <a class="main-sidebar-heading link-unstyled" href="#section-a">
                  Section A
                </a>
              </li>
              <li class="main-sidebar-item main-sidebar-item-indented">
                <a class="link-unstyled" href="#">
                  Item 1
                </a>
              </li>
              <li class="main-sidebar-item main-sidebar-item-indented">
                <a class="link-unstyled" href="#">
                  Item 2
                </a>
              </li>
              <li class="main-sidebar-main-item main-sidebar-item-indented separator">
                <a class="main-sidebar-heading link-unstyled">
                  Section B
                </a>
              </li>
              <li class="main-sidebar-item main-sidebar-item-indented">
                <a class="link-unstyled" href="#">
                  Item 1
                </a>
              </li>
              <li class="main-sidebar-item main-sidebar-item-indented">
                <a class="link-unstyled" role="button" data-toggle="collapse" href="#item-2" aria-expanded="true" aria-controls="item-2">
                  <span>Item 2</span>
                  <i class="main-sidebar-item-icon fa fa-chevron-up" aria-hidden="true"></i>
                </a>
                <div class="collapse in" id="item-2" aria-expanded="true">
                  <ul class="main-sidebar-item-submenu list-unstyled">
                    <li class="submenu-item">
                      <a class="link-unstyled" href="#" target="_self">
                        Nested Item
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
            </ul>
          </aside>
        </div>
      </div>
    `)
  },
}

