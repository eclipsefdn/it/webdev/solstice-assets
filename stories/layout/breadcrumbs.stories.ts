/*
 * Copyright (c) 2025 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import '../../less/astro/main.less';
import type { Meta, StoryObj } from '@storybook/html';
import { container, html } from '../utils';

const meta: Meta = {
  title: 'Layout/Breadcrumbs',
  decorators: [container()],
};

export default meta;

type Story = StoryObj<any>;

export const Breadcrumbs: Story = {
  render: () => {
    return html`
      <div class="default-breadcrumbs hidden-print" id="breadcrumb">
        <div class="row">
          <div class="col-sm-24">
            <ol aria-label="Breadcrumb" class="breadcrumb">
              <li>
                <a href="https://www.eclipse.org/">Home</a>
              </li>
              <li>
                <a href="https://www.eclipse.org/membership/">Eclipse Membership</a>
              </li>
              <li class="active" aria-current="page">
                <a href="https://www.eclipse.org/membership/explore-membership/">Explore Our Members</a>
              </li>
            </ol>
          </div>
        </div>
      </div>
    `;
  },
}

