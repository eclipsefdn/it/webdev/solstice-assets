/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import '../../less/astro/main.less';

import type { StoryObj } from '@storybook/html';
import { container, html } from '../utils';

export default {
  title: 'Layout/Jumbotron',
  decorators: [container('container-fluid')]
};

type Story = StoryObj<any>;

const commonOptions: Partial<Story> = {
  argTypes: {
    container: {
      control: 'text',
      description: 'The container class for the jumbotron. E.g. "container" or "container-fluid"'
    },
    class: {
      control: 'text',
      description: 'The column class for the jumbotron',
    },
    headline: {
      control: 'text',
      description: 'The headline for the jumbotron'
    },
    subtitle: {
      control: 'text',
      description: 'The subtitle for the jumbotron',
    },
    tagline: {
      control: 'text',
      description: 'The tagline for the jumbotron',
    },
    taglineClass: {
      control: 'text',
      description: 'The class for the tagline',
      if: {
        arg: 'tagline',
      }
    },
    background: {
      control: 'text',
      description: 'Background image url for the jumbotron', 
    },
  },
  args: {
    container: 'container',
    class: 'col-md-20 col-md-offset-2 col-sm-18 col-sm-offset-3',
    headline: 'Headline',
    subtitle: 'Lorem ipsum dolor sit amet, consectetur.',
    tagline: '',
    taglineClass: 'col-md-16',
    background: 'https://www.eclipse.org/public/images/vendor/eclipsefdn-solstice-components/landing-well/eclipse-home-bg.jpg',
  },
}

export const Default: Story = {
  ...commonOptions,
  render: ({ 
    class: jumbotronClass, 
    container,
    headline,
    subtitle,
    tagline,
    taglineClass,
    background,
  }) => {
    return html`
      <header class="header-wrapper">
        <div class="jumbotron featured-jumbotron margin-bottom-0" style="background-image: url('${background}');">
          <div class="${container}">
            <div class="row">
              <div class="featured-jumbotron-content ${jumbotronClass}">
                <h1 class="featured-jumbotron-headline">${headline}</h1> 
                <div class="featured-jumbotron-subtitle">
                  ${subtitle}
                </div>
                ${ tagline
                  ? html`
                    <div class="row">
                      <div class="${taglineClass}">
                        <p class="featured-jumbotron-tagline">${tagline}</p>
                      </div>
                    </div>
                  `
                  : ''
                }
                <ul class="list-inline">
                  <li>
                    <a class="btn btn-primary" href="#">
                      Text
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </header>
    `
  }
}
