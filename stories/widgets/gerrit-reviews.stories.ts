/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import '../../less/astro/main.less';

import type { StoryObj } from '@storybook/html';
import { container } from '../utils';
import '../../js/api/jquery.eclipsefdn.api';

export default {
  title: 'Widgets/Gerrit Reviews',
  decorators: [container()],
};

type Story = StoryObj<any>;

export const GerritReviews: Story = {
  argTypes: {
    username: { control: { type: 'text' }},
  },
  args: {
    id: 'gerrit-reviews',
    username: 'cguindon',
  },
  render: ({ id, username }) => {
    return (`
      <div id="${id}"></div>
      <script>
        (function($) {
          $('#${id}').eclipseFdnApi({ 
            type: 'gerritReviews', 
            username: '${username}', 
          }) 
        })(jQuery, document)
      </script>
    `)
  },
}

