/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import type { Meta, StoryObj } from '@storybook/html';
import { container, afterStoryRender, html } from '../utils';
import eclipsefdnMemberDetail from '../../js/solstice/eclipsefdn.members-detail';

const meta: Meta = {
  title: 'Widgets/Member Detail',
  decorators: [container()],
  parameters: {
    query: {
      member_id: 656,
    }
  },
};

export default meta;

type Story = StoryObj<any>;

export const MemberDetail: Story = {
  render: () => {
    afterStoryRender(eclipsefdnMemberDetail.render);
    return html`
      <div class="member-detail"></div>
    `;
  },
}

