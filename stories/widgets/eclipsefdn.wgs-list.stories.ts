/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import '../../less/astro/main.less';

import type { StoryObj } from '@storybook/html';
import { container, afterStoryRender, setOptionalAttribute } from '../utils';
import eclipsefdnWorkingGroupsList from '../../js/solstice/eclipsefdn.wgs-list';

export default {
  title: 'Widgets/Working Groups List',
  decorators: [container()],
};

type Story = StoryObj<any>;

export const WorkingGroupsList: Story = {
  argTypes: {
  },
  args: {
  },
  render: ({}) => {
    afterStoryRender(eclipsefdnWorkingGroupsList.render);

    return (`
      <div class="eclipsefdn-wgs-list">
      </div>
    `);
  },
}

