/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import '../../less/astro/main.less';

import type { StoryObj, ArgTypes } from '@storybook/html';
import { html, container, afterStoryRender, setOptionalAttribute, workingGroups, interestGroups } from '../utils';
import eclipsefdnMembersList from '../../js/solstice/eclipsefdn.members-list';

export default {
  title: 'Widgets/Members List',
  decorators: [container()],
};

type Story = StoryObj<any>;

const commonArgTypes: ArgTypes = {
  id: { control: 'select', options: [...workingGroups, ...interestGroups] },
  type: { control: 'radio', options: ['working-group', 'interest-group'] },
  sort: { control: 'radio', options: ['alphanumeric', 'random'] },
  level: { control: 'text', description: 'The participation level code (e.g. SD, AS, AP, WGSAP, etc.)' },
}

export const BoxGrid: Story = {
  argTypes: commonArgTypes,
  render: ({ level, id, type, sort }) => {
    afterStoryRender(eclipsefdnMembersList.render);

    return (`
      <div 
        class="eclipsefdn-members-list flex-wrap flex-center gap-50"
        ${setOptionalAttribute('data-id', id)}
        ${setOptionalAttribute('data-type', type)}
        ${setOptionalAttribute('data-ml-level', level)}
        ${setOptionalAttribute('data-ml-sort', sort)}
      >
      </div>
    `)
  },
}

export const LogoTitleWithLevels: Story = {
  argTypes: { 
    id: commonArgTypes.id, 
    sort: commonArgTypes.sort, 
    type: commonArgTypes.type, 
  },
  render: ({ id, type, sort }) => {
    afterStoryRender(eclipsefdnMembersList.render);
    return html`
      <div
        class="eclipsefdn-members-list"
        ${setOptionalAttribute('data-id', id)}
        ${setOptionalAttribute('data-type', type)}
        ${setOptionalAttribute('data-ml-sort', sort)}
        data-ml-template="logo-title-with-levels"
      >
      </div>
    `;
  },
}

export const OnlyLogos: Story = {
  argTypes: commonArgTypes,
  render: ({ level, id, type, sort }) => {
    afterStoryRender(eclipsefdnMembersList.render);

    return (`
      <div
        class="eclipsefdn-members-list"
        data-ml-template="only-logos"
        ${setOptionalAttribute('data-id', id)}
        ${setOptionalAttribute('data-type', type)}
        ${setOptionalAttribute('data-ml-level', level)}
        ${setOptionalAttribute('data-ml-sort', sort)}
      ></div>
    `);
  },
};

