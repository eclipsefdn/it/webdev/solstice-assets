/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import '../../less/astro/main.less';

import type { StoryObj } from '@storybook/html';
import { container, setOptionalAttribute } from '../utils';
import '../../js/api/jquery.eclipsefdn.api';

export default {
  title: 'Widgets/News Items',
  decorators: [container()],
};

type Story = StoryObj<any>;

export const NewsItems: Story = {
  argTypes: {
    id: { control: 'text' },
    newsType: { control: 'select', options: ['announcements', 'community_news'] },
    newsCount: { control: { type: 'number', min: 1 }},
    publishTarget: { control: 'select', options: ['eclipse_org', 'eclipse_iot', 'sparkplug', 'jakarta_ee', 'research', 'edge_native', 'openhwgroup', 'openmdm', 'openmobility', 'openpass', 'osgi', 'eclipse_ide', 'asciidoc', 'ospo_zone', 'oniro', 'sdv', 'adoptium', 'security'].sort() },
    pagination: { control: { type: 'boolean' }},
  },
  args: {
    newsType: 'community_news',
    newsCount: 5,
    pagination: false,
    id: 'news-list',
  },
  render: ({ newsType, newsCount, pagination, publishTarget, id }) => {
    return (`
      <div 
        class="news-items" 
        id="${id}" 
        data-news-type="${newsType}" 
        ${setOptionalAttribute('data-publish-target', publishTarget)} 
        data-news-count="${newsCount}" 
        data-pagination="${pagination}"
      >
      </div>

      <script>
        (function(e) {
          e('#${id}').eclipseFdnApi({ type: 'newsItems' }) 
        })(jQuery, document)
      </script>
    `)
  },
}
